import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';

import { TasksServiceProvider } from '../../providers/tasks-service/tasks-service';
import { UsersServiceProvider } from '../../providers/users-service/users-service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {
  ngOnInit(): void {
  }

  

  usuarios: any[];
  nomUsuario: string[] = [];

  // public properties

  tasks: any[] = [];

  // private fields 

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public tasksService: TasksServiceProvider,
    public http: TasksServiceProvider,
    public myToastCtrl: ToastController,
    public httpj: HttpClient
  ) { }

  // Ionic's or Angular's triggers

  ionViewDidEnter() {
    this.getAllTasks();

  }

  // public methods

  deleteTask(task: any, index) {
    this.tasksService.delete(task)
      .then(response => {
        console.log(response);
        this.tasks.splice(index, 1);
      })
      .catch(error => {
        console.error(error);
      })
  }

  getAllTasks() {
    this.tasksService.getAll()
      .then(tasks => {
        console.log(tasks);
        this.tasks = tasks;
      })
      .catch(error => {
        console.error(error);
      })
  }

  openAlertNewTask() {
    let alert = this.alertCtrl.create({
      title: 'Adición de Usuarios',
      message: 'Escriba el nombre del usuario',
      inputs: [
        {
          name: 'title',
          placeholder: 'Digite aquí el nombre.',
        },

        {
          name: 'description',
          placeholder: 'Digite aquí la descripcion.',
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('cancelar');
          }
        },
        {
          text: 'Guardar',
          handler: (data) => {
            //data.completed = false;
            this.tasksService.create(data)
              .then(response => {
                this.tasks.unshift(data);
              })
              .catch(error => {
                console.error(error);
              })
          }
        }
      ]
    });
    alert.present();
  }

  updateTask(task: any, index: any) {
    let prompt = this.alertCtrl.create({
      title: 'Actualizar Usuario',
      message: "Digite el nombre del usuario",
      inputs: [
        {
          name: 'title',
          value: task.title,
          placeholder: 'Usuario'
        },
        {
          name: 'description',
          value: task.description,
          placeholder: 'Descripción'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Actualizar',
          handler: data => {
            let updatetask = Object.assign({}, task);
            updatetask.title = data.title;
            updatetask.description = data.description;
            this.tasksService.update(updatetask)
              .then(response => {
                response = updatetask;
                //this.tasks[index] = response;
                this.tasks[index] = updatetask;
              })
              .catch(error => {
                console.error(error);
              });
          }
        }
      ]
    });
    prompt.present();
  }

  guardarDatos(data) {
    this.tasksService.create(data)
      .then(response => {
        this.tasks.unshift(data);
      })
      .catch(error => {
        console.error(error);
      })
  }


}
