import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TasksServiceProvider } from '../providers/tasks-service/tasks-service';
import { SQLite } from '@ionic-native/sqlite';

import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public tasksService: TasksServiceProvider,
    public sqlite: SQLite
  ) {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.createDatabase();
    });
  }

  private createDatabase() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default' // the location field is required
    })
      .then((db) => {
        this.tasksService.setDatabase(db);
        return this.tasksService.createTable();
      })
      .then(() => {
        this.splashScreen.hide();
        this.rootPage = HomePage;
      })
      .catch(error => {
        console.error(error);
      });
  }
  
}

